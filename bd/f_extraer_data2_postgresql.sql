﻿--data-historiaextraccion-stock-

select * from f_agregar_data(
				'[{"partnumber":"SDCZ51-016G-B35B",
				   "ruc":20518920465,
				   "razonsocial":"A&L PERU S.A.C.",
				   "ciudad":"LIMA",
				   "factura":"FACF103-00000034230",
				   "fecha":"2015-01-21",
				   "cantidad":3,
				   "rucmayorista":20212331377}]',
				   1
);

delete from detalleventa;
delete from venta;
delete from cliente;
delete from stock;
delete from historiaextraccion;
delete from data;

UPDATE public.correlativo SET numero=0 WHERE tabla='data';
UPDATE public.correlativo SET numero=0 WHERE tabla='detalleventa';
UPDATE public.correlativo SET numero=0 WHERE tabla='venta';
UPDATE public.correlativo SET numero=0 WHERE tabla='cliente';
UPDATE public.correlativo SET numero=0 WHERE tabla='stock';
UPDATE public.correlativo SET numero=0 WHERE tabla='historiaextraccion';

CREATE OR REPLACE FUNCTION public.f_agregar_data(
    IN p_detalle_data json, IN p_usuario integer)
  RETURNS TABLE(dataid integer) AS
$BODY$
	DECLARE

		v_dataid integer;
		v_historiaextraccion integer;
		v_stock integer;		
		v_venta integer;

		v_mayorista bigint;
		v_partnumber varchar;
		v_codigopartnumber int;
		v_ruccliente bigint;
		v_departamento char(2);
		v_provincia char(2);
		v_factura varchar;
		v_detalleventa int;
		
		v_detalle_data_cursor refcursor;
		v_detalle_data_registro record;
		
	begin	
		begin			

			select numero + 1 into v_dataid from correlativo where tabla = 'data';

			select numero + 1 into v_stock from correlativo where tabla = 'stock';

			open v_detalle_data_cursor for
				select
					partnumber,
					ruc,
					razonsocial,
					ciudad,
					factura,
					fecha,
					cantidad,
					rucmayorista
				from
					json_populate_recordset
					(
						null:: data,
						p_detalle_data
					);

			loop 
				fetch v_detalle_data_cursor into v_detalle_data_registro; 
				if found then 
					

					INSERT INTO public.data(
						    dataid, 
						    partnumber, 
						    rucmayorista, 
						    ruc, 
						    razonsocial, 
						    ciudad, 
						    factura, 
						    fecha, 
						    cantidad)
					VALUES (
							v_dataid, 
							v_detalle_data_registro.partnumber, 
							v_detalle_data_registro.rucmayorista, 
							v_detalle_data_registro.ruc,
							v_detalle_data_registro.razonsocial, 
							v_detalle_data_registro.ciudad, 
							v_detalle_data_registro.factura, 
							v_detalle_data_registro.fecha, 
							v_detalle_data_registro.cantidad);


					v_dataid= v_dataid + 1;

					v_mayorista = v_detalle_data_registro.rucmayorista;

					--inicio: stock

					select * into v_partnumber from f_buscar_producto(v_detalle_data_registro.partnumber);

					--if v_partnumber is null then 
					--RAISE EXCEPTION 'El producto (%) no se encuentra registrado en la tabla productos', v_detalle_data_registro.partnumber;
					--end if;

					if v_partnumber is not null then 

					        select productoid into v_codigopartnumber from producto where codigoproducto @@ v_partnumber;
						
						INSERT INTO public.stock(stockid, productoid, mayoristaid, cantidad, fecha)
						VALUES (v_stock, v_codigopartnumber, v_detalle_data_registro.rucmayorista, v_detalle_data_registro.cantidad, v_detalle_data_registro.fecha);

						v_stock = v_stock + 1;
						
					end if;
					
					--fin: stock

					--inicio: departamento y provincia
					SELECT departamento.departamentoid
					INTO v_departamento
					FROM public.departamento,public.provincia
					WHERE departamento.departamentoid = provincia.departamentoid
					AND provincia.nombreprovincia @@ v_detalle_data_registro.ciudad;

					SELECT provincia.provinciaid
					INTO v_provincia
					FROM public.departamento,public.provincia
					WHERE departamento.departamentoid = provincia.departamentoid
					AND provincia.nombreprovincia @@ v_detalle_data_registro.ciudad;
					--fin: departamento y provincia

					--inicio: ruc cliente
					select clienteid into v_ruccliente from cliente where clienteid = v_detalle_data_registro.ruc;

					if v_ruccliente is null then

						INSERT INTO public.cliente(clienteid, razonsocial, provinciaid, departamentoid)
						VALUES (v_detalle_data_registro.ruc, v_detalle_data_registro.razonsocial, v_provincia, v_departamento);
					end if;					
					--fin: ruc cliente

					--inicio: venta y ventadetalle

					select factura into v_factura from venta where factura @@ v_detalle_data_registro.factura;

					if v_factura is null then								
						

						select * into v_partnumber from f_buscar_producto(v_detalle_data_registro.partnumber);

						if v_partnumber is not null then 

							select numero + 1 into v_venta from correlativo where tabla = 'venta';
							

							update correlativo set numero = 0 where tabla = 'detalleventa';

							select numero + 1 into v_detalleventa from correlativo where tabla = 'detalleventa';

											
							INSERT INTO public.venta(ventaid, fecha, factura, clienteid, mayoristaid, usuarioid)
							VALUES (v_venta, v_detalle_data_registro.fecha, v_detalle_data_registro.factura, 
							v_detalle_data_registro.ruc, v_detalle_data_registro.rucmayorista, p_usuario);

							select productoid into v_codigopartnumber from producto where codigoproducto @@ v_partnumber;
							
							INSERT INTO public.detalleventa(detalleventaid, productoid, clienteid, ventaid, cantidad)
							VALUES (v_detalleventa, v_codigopartnumber, v_detalle_data_registro.ruc, v_venta, v_detalle_data_registro.cantidad);

							update correlativo set numero = v_venta where tabla = 'venta';

							update correlativo set numero = v_detalleventa where tabla = 'detalleventa';							
						end if;

												
					else
						
						select * into v_partnumber from f_buscar_producto(v_detalle_data_registro.partnumber);

						if v_partnumber is not null then 

							select numero + 1 into v_detalleventa from correlativo where tabla = 'detalleventa';

							select ventaid into v_venta from venta where factura @@ v_detalle_data_registro.factura;

							select productoid into v_codigopartnumber from producto where codigoproducto @@ v_partnumber;
							
							INSERT INTO public.detalleventa(detalleventaid, productoid, clienteid, ventaid, cantidad)
							VALUES (v_detalleventa, v_codigopartnumber, v_detalle_data_registro.ruc, v_venta, v_detalle_data_registro.cantidad);

							update correlativo set numero = v_detalleventa where tabla = 'detalleventa';
						
						end if;

						
					end if;					
							
					--fin: venta y ventadetalle
					
				else
					exit; --salir del bucle
				end if;
			end loop;

			update correlativo set numero = v_dataid-1 where tabla = 'data';

			--inicio: historia de extracción

			select numero + 1 into v_historiaextraccion from correlativo where tabla = 'historiaextraccion';

			INSERT INTO public.historiaextraccion(historiaid, mayoristaid)
			VALUES (v_historiaextraccion, v_mayorista);

			update correlativo set numero = v_historiaextraccion where tabla = 'historiaextraccion';

			--fin: historia de extracción

			update correlativo set numero = v_stock-1 where tabla = 'stock';
			
		EXCEPTION
			when others then --Cuando ocurra algun error, muestra un mensaje
				RAISE EXCEPTION '%', SQLERRM;
			
		end;

		return query select v_dataid-1;
	end;
	
$BODY$
  LANGUAGE plpgsql VOLATILE