﻿-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2017-01-19 20:50:44.002

-- tables
-- Table: categoria
CREATE TABLE categoria (
    categoriaID int  NOT NULL,
    descripcion varchar(50)  NOT NULL,
    observacion varchar(50)  NOT NULL,
    CONSTRAINT categoria_pk PRIMARY KEY  (categoriaID)
);

-- Table: cliente
CREATE TABLE cliente (
    clienteID varchar(11)  NOT NULL,
    razonSocial varchar(50)  NOT NULL,
    provinciaID char(2)  NOT NULL,
    CONSTRAINT cliente_pk PRIMARY KEY  (clienteID)
);

-- Table: correlativo
CREATE TABLE correlativo (
    tabla varchar(100)  NOT NULL,
    numero int  NULL,
    CONSTRAINT correlativo_pk PRIMARY KEY  (tabla)
);

-- Table: credito
CREATE TABLE credito (
    creditoID int  NOT NULL,
    mayoristaID varchar(11)  NOT NULL,
    saldoActual numeric(14,2)  NULL,
    saldoFavor numeric(14,2)  NULL,
    diasVencidos int  NULL,
    CONSTRAINT credito_pk PRIMARY KEY  (creditoID)
);

-- Table: departamento
CREATE TABLE departamento (
    departamentoID char(2)  NOT NULL,
    nombreDepartamento varchar(30)  NULL,
    CONSTRAINT departamento_pk PRIMARY KEY  (departamentoID)
);

-- Table: detalleOrdenDeCompra
CREATE TABLE detalleOrdenDeCompra (
    detalleID varchar  NOT NULL,
    ordenID varchar(12)  NOT NULL,
    productoID varchar(20)  NOT NULL,
    cantidad int  NOT NULL,
    precio int  NOT NULL,
    CONSTRAINT detalleOrdenDeCompra_pk PRIMARY KEY  (detalleID)
);

-- Table: detalleVenta
CREATE TABLE detalleVenta (
    detalleVentaID int  NOT NULL,
    productoID varchar(20)  NOT NULL,
    clienteID varchar(11)  NOT NULL,
    ventaID int  NOT NULL,
    cantidad int  NOT NULL,
    usuarioID int  NOT NULL,
    CONSTRAINT detalleVenta_pk PRIMARY KEY  (detalleVentaID)
);

-- Table: formasPago
CREATE TABLE formasPago (
    pagoID int  NOT NULL,
    tipoPago varchar(20)  NULL,
    CONSTRAINT formasPago_pk PRIMARY KEY  (pagoID)
);

-- Table: historiaExtraccion
CREATE TABLE historiaExtraccion (
    historiaID int  NOT NULL,
    mayoristaID varchar(11)  NOT NULL,
    fecha date  NULL,
    estado char(1)  NULL,
    CONSTRAINT historiaExtraccion_pk PRIMARY KEY  (historiaID)
);

-- Table: mayorista
CREATE TABLE mayorista (
    mayoristaID varchar(11)  NOT NULL,
    razonSocial varchar(50)  NULL,
    email varchar(20)  NOT NULL,
    telefono varchar(12)  NOT NULL,
    webService varchar(200)  NOT NULL,
    direccion varchar(50)  NOT NULL,
    provinciaID char(2)  NOT NULL,
    CONSTRAINT mayorista_pk PRIMARY KEY  (mayoristaID)
);

-- Table: ordenDeCompra
CREATE TABLE ordenDeCompra (
    ordenID varchar(12)  NOT NULL,
    mayoristaID varchar(11)  NOT NULL,
    pagoID int  NOT NULL,
    fechaEmitida date  NOT NULL,
    fechaPago date  NOT NULL,
    estado char(2)  NOT NULL,
    CONSTRAINT ordenDeCompra_pk PRIMARY KEY  (ordenID)
);

-- Table: personal
CREATE TABLE personal (
    dni int  NOT NULL,
    apellidoPaterno varchar(30)  NULL,
    apellidoMaterno varchar(30)  NULL,
    nombre varchar(30)  NULL,
    cargo varchar(20)  NULL,
    CONSTRAINT personal_pk PRIMARY KEY  (dni)
);

-- Table: producto
CREATE TABLE producto (
    productoID varchar(20)  NOT NULL,
    modelo varchar(50)  NULL,
    descripcion varchar(50)  NULL,
    capacidad varchar(30)  NULL,
    estado char(1)  NULL,
    tipoCategoriaID int  NOT NULL,
    CONSTRAINT producto_pk PRIMARY KEY  (productoID)
);

-- Table: provincia
CREATE TABLE provincia (
    provinciaID char(2)  NOT NULL,
    nombreProvincia varchar(30)  NULL,
    departamentoID char(2)  NOT NULL,
    CONSTRAINT provincia_pk PRIMARY KEY  (provinciaID)
);

-- Table: stock
CREATE TABLE stock (
    stockID varchar(20)  NOT NULL,
    productoID varchar(20)  NOT NULL,
    mayoristaID varchar(11)  NOT NULL,
    cantidad int  NULL,
    fecha date  NULL,
    CONSTRAINT stock_pk PRIMARY KEY  (stockID)
);

-- Table: tipoCategoria
CREATE TABLE tipoCategoria (
    tipoCategoriaID int  NOT NULL,
    categoriaID int  NOT NULL,
    descripcion varchar(50)  NOT NULL,
    comentario varchar(50)  NOT NULL,
    CONSTRAINT tipoCategoria_pk PRIMARY KEY  (tipoCategoriaID)
);

-- Table: usuario
CREATE TABLE usuario (
    usuarioID int  NOT NULL,
    personal_dni int  NOT NULL,
    clave char(32)  NULL,
    estado char(1)  NULL,
    CONSTRAINT usuario_pk PRIMARY KEY  (usuarioID)
);

-- Table: vendedor
CREATE TABLE vendedor (
    vendedorID varchar(9)  NOT NULL,
    nombre varchar(50)  NULL,
    apellidoPaterno varchar(50)  NULL,
    apellidoMaterno varchar(50)  NULL,
    direccion varchar(50)  NULL,
    telefono varchar(20)  NULL,
    email varchar(40)  NULL,
    fechaNacimiento date  NULL,
    fechaIngreso date  NULL,
    fechaTermino date  NULL,
    estado char(1)  NULL,
    CONSTRAINT vendedor_pk PRIMARY KEY  (vendedorID)
);

-- Table: vendedorDetalle
CREATE TABLE vendedorDetalle (
    vendedorDetalleID int  NOT NULL,
    vendedorID varchar(9)  NOT NULL,
    clienteID varchar(11)  NOT NULL,
    estado char(1)  NULL,
    CONSTRAINT vendedorDetalle_pk PRIMARY KEY  (vendedorDetalleID)
);

-- Table: venta
CREATE TABLE venta (
    ventaID int  NOT NULL,
    vendedorID varchar(9)  NOT NULL,
    fecha date  NOT NULL,
    factura varchar(13)  NOT NULL,
    clienteID varchar(11)  NOT NULL,
    mayoristaID varchar(11)  NOT NULL,
    CONSTRAINT venta_pk PRIMARY KEY  (ventaID)
);

-- foreign keys
-- Reference: cliente_provincia (table: cliente)
ALTER TABLE cliente ADD CONSTRAINT cliente_provincia
    FOREIGN KEY (provinciaID)
    REFERENCES provincia (provinciaID);

-- Reference: credito_mayorista (table: credito)
ALTER TABLE credito ADD CONSTRAINT credito_mayorista
    FOREIGN KEY (mayoristaID)
    REFERENCES mayorista (mayoristaID);

-- Reference: detalleOrdenDeCompra_ordenDeCompra (table: detalleOrdenDeCompra)
ALTER TABLE detalleOrdenDeCompra ADD CONSTRAINT detalleOrdenDeCompra_ordenDeCompra
    FOREIGN KEY (ordenID)
    REFERENCES ordenDeCompra (ordenID);

-- Reference: detalleOrdenDeCompra_producto (table: detalleOrdenDeCompra)
ALTER TABLE detalleOrdenDeCompra ADD CONSTRAINT detalleOrdenDeCompra_producto
    FOREIGN KEY (productoID)
    REFERENCES producto (productoID);

-- Reference: detalleVenta_cliente (table: detalleVenta)
ALTER TABLE detalleVenta ADD CONSTRAINT detalleVenta_cliente
    FOREIGN KEY (clienteID)
    REFERENCES cliente (clienteID);

-- Reference: detalleVenta_producto (table: detalleVenta)
ALTER TABLE detalleVenta ADD CONSTRAINT detalleVenta_producto
    FOREIGN KEY (productoID)
    REFERENCES producto (productoID);

-- Reference: detalleVenta_usuario (table: detalleVenta)
ALTER TABLE detalleVenta ADD CONSTRAINT detalleVenta_usuario
    FOREIGN KEY (usuarioID)
    REFERENCES usuario (usuarioID);

-- Reference: detalleVenta_venta (table: detalleVenta)
ALTER TABLE detalleVenta ADD CONSTRAINT detalleVenta_venta
    FOREIGN KEY (ventaID)
    REFERENCES venta (ventaID);

-- Reference: historiaExtraccion_mayorista (table: historiaExtraccion)
ALTER TABLE historiaExtraccion ADD CONSTRAINT historiaExtraccion_mayorista
    FOREIGN KEY (mayoristaID)
    REFERENCES mayorista (mayoristaID);

-- Reference: mayorista_provincia (table: mayorista)
ALTER TABLE mayorista ADD CONSTRAINT mayorista_provincia
    FOREIGN KEY (provinciaID)
    REFERENCES provincia (provinciaID);

-- Reference: ordenDeCompra_formasPago (table: ordenDeCompra)
ALTER TABLE ordenDeCompra ADD CONSTRAINT ordenDeCompra_formasPago
    FOREIGN KEY (pagoID)
    REFERENCES formasPago (pagoID);

-- Reference: ordenDeCompra_mayorista (table: ordenDeCompra)
ALTER TABLE ordenDeCompra ADD CONSTRAINT ordenDeCompra_mayorista
    FOREIGN KEY (mayoristaID)
    REFERENCES mayorista (mayoristaID);

-- Reference: producto_tipoCategoria (table: producto)
ALTER TABLE producto ADD CONSTRAINT producto_tipoCategoria
    FOREIGN KEY (tipoCategoriaID)
    REFERENCES tipoCategoria (tipoCategoriaID);

-- Reference: provincia_departamento (table: provincia)
ALTER TABLE provincia ADD CONSTRAINT provincia_departamento
    FOREIGN KEY (departamentoID)
    REFERENCES departamento (departamentoID);

-- Reference: stock_mayorista (table: stock)
ALTER TABLE stock ADD CONSTRAINT stock_mayorista
    FOREIGN KEY (mayoristaID)
    REFERENCES mayorista (mayoristaID);

-- Reference: stock_producto (table: stock)
ALTER TABLE stock ADD CONSTRAINT stock_producto
    FOREIGN KEY (productoID)
    REFERENCES producto (productoID);

-- Reference: tipoCategoria_categoria (table: tipoCategoria)
ALTER TABLE tipoCategoria ADD CONSTRAINT tipoCategoria_categoria
    FOREIGN KEY (categoriaID)
    REFERENCES categoria (categoriaID);

-- Reference: usuario_personal (table: usuario)
ALTER TABLE usuario ADD CONSTRAINT usuario_personal
    FOREIGN KEY (personal_dni)
    REFERENCES personal (dni);

-- Reference: vendedorDetalle_cliente (table: vendedorDetalle)
ALTER TABLE vendedorDetalle ADD CONSTRAINT vendedorDetalle_cliente
    FOREIGN KEY (clienteID)
    REFERENCES cliente (clienteID);

-- Reference: vendedorDetalle_vendedor (table: vendedorDetalle)
ALTER TABLE vendedorDetalle ADD CONSTRAINT vendedorDetalle_vendedor
    FOREIGN KEY (vendedorID)
    REFERENCES vendedor (vendedorID);

-- Reference: venta_cliente (table: venta)
ALTER TABLE venta ADD CONSTRAINT venta_cliente
    FOREIGN KEY (clienteID)
    REFERENCES cliente (clienteID);

-- Reference: venta_mayorista (table: venta)
ALTER TABLE venta ADD CONSTRAINT venta_mayorista
    FOREIGN KEY (mayoristaID)
    REFERENCES mayorista (mayoristaID);

-- Reference: venta_vendedor (table: venta)
ALTER TABLE venta ADD CONSTRAINT venta_vendedor
    FOREIGN KEY (vendedorID)
    REFERENCES vendedor (vendedorID);

-- End of file.

