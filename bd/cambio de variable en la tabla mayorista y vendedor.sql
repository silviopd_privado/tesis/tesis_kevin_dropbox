﻿--cambiar de varchar a int la tabla mayorista
ALTER TABLE STOCK DROP CONSTRAINT stock_mayorista;
ALTER TABLE historiaextraccion DROP CONSTRAINT historiaextraccion_mayorista;
ALTER TABLE credito DROP CONSTRAINT credito_mayorista;
ALTER TABLE venta DROP CONSTRAINT venta_mayorista;
ALTER TABLE ordendecompra DROP CONSTRAINT ordendecompra_mayorista;

ALTER TABLE STOCK ALTER COLUMN mayoristaid TYPE integer USING mayoristaid::int;
ALTER TABLE historiaextraccion ALTER COLUMN mayoristaid TYPE integer USING mayoristaid::int;
ALTER TABLE credito ALTER COLUMN mayoristaid TYPE integer USING mayoristaid::int;
ALTER TABLE venta ALTER COLUMN mayoristaid TYPE integer USING mayoristaid::int;
ALTER TABLE ordendecompra ALTER COLUMN mayoristaid TYPE integer USING mayoristaid::int;

ALTER TABLE mayorista ALTER COLUMN mayoristaid TYPE integer USING mayoristaid::int;


ALTER TABLE STOCK ADD CONSTRAINT stock_mayorista FOREIGN KEY (mayoristaid) REFERENCES mayorista (mayoristaid);
ALTER TABLE historiaextraccion ADD CONSTRAINT historiaextraccion_mayorista FOREIGN KEY (mayoristaid) REFERENCES mayorista (mayoristaid);
ALTER TABLE credito ADD CONSTRAINT credito_mayorista FOREIGN KEY (mayoristaid) REFERENCES mayorista (mayoristaid);
ALTER TABLE venta ADD CONSTRAINT venta_mayorista FOREIGN KEY (mayoristaid) REFERENCES mayorista (mayoristaid);
ALTER TABLE ordendecompra ADD CONSTRAINT ordendecompra_mayorista FOREIGN KEY (mayoristaid) REFERENCES mayorista (mayoristaid);

-------------------------------------------------


ALTER TABLE venta DROP CONSTRAINT venta_cliente;
ALTER TABLE detalleventa DROP CONSTRAINT detalleventa_cliente;
ALTER TABLE vendedordetalle DROP CONSTRAINT vendedordetalle_cliente;

ALTER TABLE venta ALTER COLUMN clienteid TYPE varchar(11);
ALTER TABLE detalleventa ALTER COLUMN clienteid TYPE varchar(11);
ALTER TABLE vendedordetalle ALTER COLUMN clienteid TYPE varchar(11);

ALTER TABLE cliente ALTER COLUMN clienteid TYPE varchar(11);

ALTER TABLE venta ADD CONSTRAINT venta_cliente FOREIGN KEY (clienteid) REFERENCES cliente (clienteid);
ALTER TABLE detalleventa ADD CONSTRAINT detalleventa_cliente FOREIGN KEY (clienteid) REFERENCES cliente (clienteid);
ALTER TABLE vendedordetalle ADD CONSTRAINT vendedordetalle_cliente FOREIGN KEY (clienteid) REFERENCES cliente (clienteid);
