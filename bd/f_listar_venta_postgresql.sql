﻿select * from f_listar_venta('01-01-1992','01-01-1992',3)

CREATE OR REPLACE FUNCTION public.f_listar_venta(
    IN p_fecha1 date,
    IN p_fecha2 date,
    IN p_tipo integer)
  RETURNS TABLE(ordenid integer, razonsocial varchar, usuario varchar,tipopago varchar, total numeric, fechaemitida date, estado varchar) AS
$BODY$
	begin
	  --consulta sql Select
	  return query
			  SELECT 
		  ordendecompra.ordenid as orde, 
		  mayorista.razonsocial as ra, 
		  (personal.apellidopaterno || ' ' || personal.apellidomaterno || ' ' || personal.nombre)::varchar as us, 
		  formaspago.tipopago as tp, 
		  ordendecompra.total as to, 
		  ordendecompra.fechaemitida as fec,
		  (case ordendecompra.estado when 'EM' then 'EMITIDO' when 'PA' then 'PAGADO' when 'CA' then 'CANCELADO' end)::varchar as esta
		FROM 
		  public.ordendecompra, 
		  public.detalleordendecompra, 
		  public.mayorista, 
		  public.usuario, 
		  public.personal, 
		  public.formaspago
		WHERE 
		  ordendecompra.ordenid = detalleordendecompra.ordenid AND
		  mayorista.mayoristaid = ordendecompra.mayoristaid AND
		  usuario.usuarioid = ordendecompra.usuarioid AND
		  usuario.personal_dni = personal.dni AND
		  formaspago.pagoid = ordendecompra.pagoid

		  and 
                  (
		     case p_tipo
                       when 1 then ordendecompra.fechaemitida = current_date --solo hoy
                       when 2 then ordendecompra.fechaemitida >= p_fecha1 and ordendecompra.fechaemitida <= p_fecha2 --por rango de fechas
		       else
			     true --las ventas de todas las fechas
		     end
		  )
		;

	end;
$BODY$
  LANGUAGE plpgsql VOLATILE