﻿SELECT 
                    ordendecompra.ordenid, 
                    ordendecompra.fechaemitida, 
                    ordendecompra.fechapago, 
                    (case ordendecompra.estado when 'EM' then 'EMITIDO' when 'PA' then 'PAGADO' else 'CANCELADO' end)::varchar as estado, 
                    upper(formaspago.tipopago)::varchar as tipopago, 
                    ordendecompra.porcentaje_igv, 
                    ordendecompra.sub_total, 
                    ordendecompra.igv, 
                    (ordendecompra.sub_total + ordendecompra.igv)::numeric as total,
                    ordendecompra.total total_pagar, 
                    (select credito.saldoactual from public.credito where credito.mayoristaid = mayorista.mayoristaid order by creditoid desc limit 1) as saldoactual,
                    (select credito.saldoactual from public.credito where credito.mayoristaid = mayorista.mayoristaid order by creditoid desc limit 1) as saldoanterior,
                    mayorista.razonsocial, 
                    detalleordendecompra.detalleid, 
                    producto.codigoproducto, 
                    detalleordendecompra.cantidad, 
                    detalleordendecompra.precio,
                    (detalleordendecompra.cantidad * detalleordendecompra.precio)::numeric(14,2) as importe
                  FROM 
                    public.detalleordendecompra, 
                    public.ordendecompra, 
                    public.producto, 
                    public.formaspago, 
                    public.mayorista
                  WHERE 
                    ordendecompra.ordenid = detalleordendecompra.ordenid AND
                    producto.productoid = detalleordendecompra.productoid AND
                    formaspago.pagoid = ordendecompra.pagoid AND
                    mayorista.mayoristaid = ordendecompra.mayoristaid
                  ORDER BY 
                    ordendecompra.ordenid desc;

                    select credito.creditoid,credito.saldoactual,credito.saldofavor from public.credito where credito.mayoristaid = 20212331377 and credito.creditoid = 33  order by creditoid desc limit 2