﻿select * from f_agregar_mayorista(11111111111,'asdfasdf','asdfasdfasdf','asdfasdfdas','','dsfgdsfg','12','12',20000)

CREATE OR REPLACE FUNCTION public.f_agregar_mayorista(
    IN p_mayoristaid bigint,
    IN p_razonsocial varchar,
    IN p_email varchar,
    IN p_telefono varchar,
    IN p_webservice varchar,
    IN p_direccion varchar,
    IN p_provinciaid char(2),
    IN p_departamentoid char(2),
    IN p_credito numeric )
  RETURNS TABLE(mayorista bigint, credito integer) AS
$BODY$
	DECLARE

		v_credito integer;
		v_buscar integer;
		
	begin	

		begin

			INSERT INTO public.mayorista(mayoristaid, razonsocial, email, telefono, webservice, direccion,provinciaid,departamentoid)
			VALUES (p_mayoristaid, p_razonsocial, p_email, p_telefono, p_webservice, p_direccion, p_provinciaid, p_departamentoid);

			select saldoactual into v_buscar from credito where mayoristaid = p_mayoristaid order by 1 desc limit 1 ; 
			
			if v_buscar > 0 then
						RAISE EXCEPTION 'El mayorista (%) ya cuenta con credito', p_mayoristaid;
			end if;


			select numero + 1 into v_credito from correlativo where tabla = 'credito';

			INSERT INTO public.credito(creditoid, mayoristaid, saldoactual, diasvencidos)
			VALUES (v_credito, p_mayoristaid, p_credito, 45);

			update correlativo set numero = v_numero_venta where tabla = 'credito';
			
		EXCEPTION
			when others then --Cuando ocurra algun error, muestra un mensaje
				RAISE EXCEPTION '%', SQLERRM;
			
		end;

		return query select p_mayoristaid,v_credito;
	end;
	
$BODY$
  LANGUAGE plpgsql VOLATILE