﻿CREATE OR REPLACE FUNCTION public.f_agregar_producto(
    IN p_codigoproducto character varying,
    IN p_modelo character varying,
    IN p_descripcion character varying,
    IN p_capacidad character varying,
    IN p_tipocategoriaid integer)
  RETURNS TABLE(productoid integer, codigoproducto character varying) AS
$BODY$
	DECLARE

		v_productoid integer;
		v_codigoproducto varchar;
		
	begin	
		begin

			select * into v_codigoproducto from f_buscar_producto(rtrim(p_codigoproducto,' '));
			
			if v_codigoproducto is null then 

				select numero + 1 into v_productoid from correlativo where tabla = 'producto';

				INSERT INTO public.producto(productoid,codigoproducto, modelo, descripcion, capacidad, tipocategoriaid)
				VALUES (v_productoid, p_codigoproducto, p_modelo,p_descripcion, p_capacidad, p_tipocategoriaid);

				update correlativo set numero = numero + 1 where tabla = 'producto';
			else
				RAISE EXCEPTION 'El producto (%) ya se encuentra agregado', p_codigoproducto;
			end if;

			
			
		EXCEPTION
			when others then --Cuando ocurra algun error, muestra un mensaje
				RAISE EXCEPTION '%', SQLERRM;
			
		end;

		return query select v_productoid,p_codigoproducto;
	end;
	
$BODY$
  LANGUAGE plpgsql VOLATILE