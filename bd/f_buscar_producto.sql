﻿CREATE OR REPLACE FUNCTION public.f_buscar_producto(IN p_partnumber character varying)
  RETURNS TABLE(partnumber character varying) AS
$BODY$
	declare
		v_producto varchar;
		
	begin
		begin
			select rtrim(producto.codigoproducto,' ')::varchar 
			into v_producto
			from producto 
			where producto.codigoproducto @@ p_partnumber;
		end;

		return query select v_producto;
	end
$BODY$
  LANGUAGE plpgsql VOLATILE;



  select * from f_buscar_producto('SDCZ51-016G-B35K')