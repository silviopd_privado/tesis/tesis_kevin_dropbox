﻿delete from provincia

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (1,1,'Chachapoyas');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (1,2,'Bagua');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (1,3,'Bongara');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (1,4,'Condorcanqui');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (1,5,'Luya');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (1,6,'Rodriguez De Mendo');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (1,7,'Utcubamba');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (2,1,'Huaraz');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (2,2,'Aija');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (2,3,'Antonio Raymondi');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (2,4,'Asuncion');
insert into provincia(departamentoid, provinciaid,nombreprovincia) values (2,5,'Bolognesi');
insert into provincia(departamentoid, provinciaid,nombreprovincia) values (2,6,'Carhuaz');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (2,7,'Carlos F. Fitzcarrald');
insert into provincia(departamentoid, provinciaid,nombreprovincia) values (2,8,'Casma');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (2,9,'Corongo');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (2,10,'Huari');
 
insert into provincia(departamentoid, provinciaid,nombreprovincia) values (2,11,'Huarmey');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (2,12,'Huaylas');
 

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (2,13,'Mariscal Luzuriaga');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (2,14,'Ocros');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (2,15,'Pallasca');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (2,16,'Pomabamba');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (2,17,'Recuay');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (2,18,'Santa');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (2,19,'Sihuas');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (2,20,'Yungay');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (3,1,'Abancay');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (3,2,'Andahuaylas');
insert into provincia(departamentoid, provinciaid,nombreprovincia) values (3,3,'Antabamba');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (3,4,'Aymaraes');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (3,5,'Cotabambas');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (3,6,'Chincheros');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (3,7,'Grau');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (4,1,'Arequipa');
insert into provincia(departamentoid, provinciaid,nombreprovincia) values (4,2,'Camana');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (4,3,'Caraveli');
insert into provincia(departamentoid, provinciaid,nombreprovincia) values (4,4,'Castilla');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (4,5,'Caylloma');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (4,6,'Condesuyos');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (4,7,'Islay');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (4,8,'La Union');
insert into provincia(departamentoid, provinciaid,nombreprovincia) values (5,1,'Huamanga');
    
insert into provincia(departamentoid, provinciaid,nombreprovincia) values (5,2,'Cangallo');
   
insert into provincia(departamentoid, provinciaid,nombreprovincia) values (5,3,'Huanca Sancos');
   
insert into provincia(departamentoid, provinciaid,nombreprovincia) values (5,4,'Huanta');
 
 
insert into provincia(departamentoid, provinciaid,nombreprovincia) values (5,5,'La Mar');
 

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (5,6,'Lucanas');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (5,7,'Parinacochas');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (5,8,'Paucar Del Sara Sara');
 
   
insert into provincia(departamentoid, provinciaid,nombreprovincia) values (5,9,'Sucre');

   
insert into provincia(departamentoid, provinciaid,nombreprovincia) values (5,10,'Victor Fajardo');

   
insert into provincia(departamentoid, provinciaid,nombreprovincia) values (5,11,'Vilcas Huaman');

 
insert into provincia(departamentoid, provinciaid,nombreprovincia) values (6,1,'Cajamarca');
 
 
insert into provincia(departamentoid, provinciaid,nombreprovincia) values (6,2,'Cajabamba');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (6,3,'Celendin');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (6,4,'Chota');
 

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (6,5,'Contumaza');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (6,6,'Cutervo');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (6,7,'Hualgayoc');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (6,8,'Jaen');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (6,9,'San Ignacio');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (6,10,'San Marcos');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (6,11,'San Miguel');

 
insert into provincia(departamentoid, provinciaid,nombreprovincia) values (6,12,'San Pablo');


 
insert into provincia(departamentoid, provinciaid,nombreprovincia) values (6,13,'Santa Cruz');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (7,1,'Cuzco');
 

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (7,2,'Acomayo');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (7,3,'Anta');
 

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (7,4,'Calca');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (7,5,'Canas');
 

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (7,6,'Canchis');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (7,7,'Chumbivilcas');
 

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (7,8,'Espinar');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (7,9,'La Convencion');
 

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (7,10,'Paruro');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (7,11,'Paucartambo');
 

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (7,12,'Quispicanchi');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (7,13,'Urubamba');
 

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (8,1,'Huancavelica');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (8,2,'Acobamba');
 
insert into provincia(departamentoid, provinciaid,nombreprovincia) values (8,3,'Angaraes');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (8,4,'Castrovirreyna');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (8,5,'Churcampa');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (8,6,'Huaytara');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (8,7,'Tayacaja');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (9,1,'Huanuco');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (9,2,'Ambo');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (9,3,'Dos De Mayo');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (9,4,'Huacaybamba');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (9,5,'Huamalies');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (9,6,'Leoncio Prado');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (9,7,'Marañon');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (9,8,'Pachitea');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (9,9,'Puerto Inca');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (9,10,'Lauricocha');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (9,11,'Yarowilca');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (10,1,'Ica');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (10,2,'Chincha');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (10,3,'Nazca');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (10,4,'Palpa');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (10,5,'Pisco');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (11,1,'Huancayo');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (11,2,'Concepcion');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (11,3,'Chanchamayo');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (11,4,'Jauja');
 

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (11,5,'Junin');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (11,6,'Satipo');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (11,7,'Tarma');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (11,8,'Yauli');
insert into provincia(departamentoid, provinciaid,nombreprovincia) values (11,9,'Chupaca');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (12,1,'Trujillo');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (12,2,'Ascope');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (12,3,'Bolivar');
insert into provincia(departamentoid, provinciaid,nombreprovincia) values (12,4,'Chepen');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (12,5,'Julcan');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (12,6,'Otuzco');
 

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (12,7,'Pacasmayo');
 
insert into provincia(departamentoid, provinciaid,nombreprovincia) values (12,8,'Pataz');
 

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (12,9,'Sanchez Carrion');

 
insert into provincia(departamentoid, provinciaid,nombreprovincia) values (12,10,'Santiago De Chuco');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (12,11,'Gran Chimu');
 

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (12,12,'Viru');
 

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (13,1,'Chiclayo');
 

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (13,2,'Ferreñafe');
 

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (13,3,'Lambayeque');
 

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (14,1,'Lima (1');
 

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (14,2,'Barranca');
 
insert into provincia(departamentoid, provinciaid,nombreprovincia) values (14,3,'Cajatambo');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (14,4,'Callao');
 

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (14,5,'Canta');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (14,6,'Cañete');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (14,7,'Huaral');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (14,8,'Huarochiri');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (14,9,'Huaura');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (14,10,'Oyon');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (14,11,'Yauyos');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (15,1,'Maynas');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (15,2,'Alto Amazonas');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (15,3,'Loreto');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (15,4,'Mariscal Ramon Castilla');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (15,5,'Requena');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (15,6,'Ucayali');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (16,1,'Tambopata');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (16,2,'Manu');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (16,3,'Tahuamanu');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (17,1,'Mariscal Nieto');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (17,2,'General Sanchez Cerro');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (17,3,'Ilo');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (18,1,'Pasco');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (18,2,'Daniel Alcides Carrion');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (18,3,'Oxapampa');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (19,1,'Piura');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (19,2,'Ayabaca');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (19,3,'Huancabamba');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (19,4,'Morropon');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (19,5,'Paita');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (19,6,'Sullana');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (19,7,'Talara');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (19,8,'Sechura');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (20,1,'Puno');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (20,2,'Azangaro');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (20,3,'Carabaya');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (20,4,'Chucuito');
insert into provincia(departamentoid, provinciaid,nombreprovincia) values (20,5,'El Collao');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (20,6,'Huancane');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (20,7,'Lampa');
insert into provincia(departamentoid, provinciaid,nombreprovincia) values (20,8,'Melgar');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (20,9,'Moho');
insert into provincia(departamentoid, provinciaid,nombreprovincia) values (20,10,'San Antonio De Putina');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (20,11,'San Roman');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (20,12,'Sandia');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (20,13,'Yunguyo');

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (21,1,'Moyobamba');
insert into provincia(departamentoid, provinciaid,nombreprovincia) values (21,2,'Bellavista');
 

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (21,3,'El Dorado');
 

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (21,4,'Huallaga');
 

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (21,5,'Lamas');
 
insert into provincia(departamentoid, provinciaid,nombreprovincia) values (21,6,'Mariscal Caceres');
 
insert into provincia(departamentoid, provinciaid,nombreprovincia) values (21,7,'Picota');
 
insert into provincia(departamentoid, provinciaid,nombreprovincia) values (21,8,'Rioja');
 

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (21,9,'San Martin');
 

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (21,10,'Tocache');
 

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (22,1,'Tacna');
 

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (22,2,'Candarave');
 

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (22,3,'Jorge Basadre');
 

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (22,4,'Tarata');
 

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (23,1,'Tumbes');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (23,2,'Contralmirante Villar');
 

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (23,3,'Zarumilla');
 

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (24,1,'Coronel Portillo');
 
insert into provincia(departamentoid, provinciaid,nombreprovincia) values (24,2,'Atalaya');
 

insert into provincia(departamentoid, provinciaid,nombreprovincia) values (24,3,'Padre Abad');


insert into provincia(departamentoid, provinciaid,nombreprovincia) values (24,4,'Purus');
 
