﻿select * from f_listar_mayorista()

CREATE OR REPLACE FUNCTION public.f_listar_mayorista()
  RETURNS TABLE(ruc bigint, razonsocial character varying, email character varying, telefono character varying, direccion character varying, webservice character varying, estado character varying, provincia character varying, departamento character varying, saldoactual numeric) AS
$BODY$
		
	begin
		return query
		SELECT 
                    distinct mayorista.mayoristaid as rc, 
                    mayorista.razonsocial as rs, 
                    mayorista.email as em, 
                    mayorista.telefono as em, 
                    mayorista.direccion as di, 
                    mayorista.webservice as ws, 
                    (case mayorista.estado when 'A' then 'ACTIVO' else 'INACTIVO' end)::varchar as est, 
                    provincia.nombreprovincia as pro,
                    departamento.nombredepartamento as dep,
                    (select credito.saldoactual from public.credito where credito.mayoristaid = mayorista.mayoristaid order by 1 desc limit 1) as sa
                  FROM 
                    public.mayorista, 
                    public.provincia, 
                    public.departamento,
                    public.credito
                  WHERE 
                    provincia.provinciaid = mayorista.provinciaid AND
                    provincia.departamentoid = mayorista.departamentoid AND
                    provincia.departamentoid = departamento.departamentoid AND 
                    credito.mayoristaid = mayorista.mayoristaid;
	end
$BODY$
  LANGUAGE plpgsql VOLATILE