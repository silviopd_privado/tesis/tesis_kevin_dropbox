﻿CREATE OR REPLACE FUNCTION public.f_registrar_venta(
    IN p_mayoristaid bigint,
    IN p_pagoid integer,
    IN p_fecha_emitida date,
    IN p_codigo_usuario integer,
    IN p_detalle_venta json)
  RETURNS TABLE(nv integer) AS
$BODY$
	declare
		v_numero_venta integer;
		v_numero_documento integer;
		v_porcentaje_igv numeric;
		v_credito integer;
		v_saldoactual numeric;

		v_detalle_venta_cursor refcursor;
		v_detalle_venta_registro record;

		v_importe_bruto numeric;
		v_descuento1 numeric;
		v_descuento2 numeric;
		v_importe numeric;

		v_item integer;

		v_sub_total numeric;
		v_igv numeric;
		v_total numeric;

		v_stock_actual integer;

	begin
		begin
	
			--Inicio: generar el nuevo numero de venta
			select numero + 1 into v_numero_venta 
			from correlativo where tabla = 'ordendecompra';

			RAISE NOTICE 'Nuevo número de venta: %', v_numero_venta;
			--Fin: generar el nuevo numero de venta

						
			--Inicio: obtener el porcentaje de IGV
			
			select 
				valor::numeric(7,2) into v_porcentaje_igv 
			from 
				configuracion 
			where 
				codigo = 1;
			
			RAISE NOTICE 'Porcentaje de IGV: %', v_porcentaje_igv;
			--Fin: obtener el porcentaje de IGV

			
			--Inicio: Insertar en la tabla venta
			
			INSERT INTO public.ordendecompra(
				ordenid, 
				mayoristaid, 
				pagoid, 
				fechaemitida,
				usuarioid, 
				porcentaje_igv, 
				sub_total, 
				igv, 
				total)
			VALUES (
				v_numero_venta, 
				p_mayoristaid, 
				p_pagoid, 
				p_fecha_emitida, 
				p_codigo_usuario, 
				v_porcentaje_igv, 
				0,
				0,
				0);


			--Fin: Insertar en la tabla venta



			--Inicio: Insertar en venta_detalle
			open v_detalle_venta_cursor for
				select
					productoid,
					cantidad,
					precio
				from
					json_populate_recordset
					(
						null:: detalleordendecompra,
						p_detalle_venta
					);

			v_item = 0; --inicializando la variable en cero
			v_total = 0; --inicializando la variable en cero

			loop --Bucle para recorrer todos los registros almacenados en la vaiable: v_detalle_venta_cursor
				fetch v_detalle_venta_cursor into v_detalle_venta_registro; --captura registro por registro
				if found then --Si aun encuentra registros
					
					
					if v_detalle_venta_registro.cantidad <= 0 then
						RAISE EXCEPTION 'La cantidad del artículo con código (%) debe ser mayor que cero', v_detalle_venta_registro.codigo_articulo;
					end if;
					
	
					v_importe_bruto = v_detalle_venta_registro.cantidad  * v_detalle_venta_registro.precio;
					v_importe = v_importe_bruto;

					v_total = v_total + v_importe;

					v_item = v_item + 1;

					INSERT INTO public.detalleordendecompra(
							detalleid, 
							ordenid, 
							productoid, 
							cantidad, 
							precio)
					VALUES (
							v_item, v_numero_venta, 
							v_detalle_venta_registro.productoid, 
							v_detalle_venta_registro.cantidad, 
							v_detalle_venta_registro.precio);

				else
					exit; --salir del bucle
				end if;
			end loop;
			--Fin: Insertar en venta_detalle

			--Inicio: Calcular y actualizar el sub total y el igv
			v_igv = v_total*(v_porcentaje_igv/100);			
			v_sub_total = v_total;
			v_total = v_sub_total+v_igv;
			

				--tabla: credito
				select numero + 1 into v_credito from correlativo where tabla = 'credito';

				select saldoactual into v_saldoactual from credito where mayoristaid = p_mayoristaid order by creditoid desc limit 1;

				if v_saldoactual >= v_total then v_total = 0; v_saldoactual=v_saldoactual-v_total; else v_total=v_total-v_saldoactual; v_saldoactual=0; end if;
			
				INSERT INTO public.credito(creditoid, mayoristaid, saldoactual, saldofavor, diasvencidos)
				VALUES (v_credito, p_mayoristaid, v_saldoactual, (-1)*v_total, 45);

				update correlativo set numero = v_credito where tabla = 'credito';
				--tabla: credito


			update 
				ordendecompra
			set
				sub_total = v_sub_total,
				igv = v_igv,
				total = v_total
			where
				ordenid = v_numero_venta;
			--Fin: Calcular y actualizar el sub total y el igv


			--Inicio: Actualizar los correlativos
			update correlativo set numero = v_numero_venta where tabla = 'ordendecompra';		
			--Fin: Actualizar los correlativos

			

			

		EXCEPTION
			when others then --Cuando ocurra algun error, muestra un mensaje
				RAISE EXCEPTION '%', SQLERRM;
		end;

		--return 1; --Cuando la transacción terminó con exito
		return query select v_numero_venta;
	end;
$BODY$
  LANGUAGE plpgsql VOLATILE