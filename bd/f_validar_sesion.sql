CREATE FUNCTION f_validar_sesion (@p_dni int,@p_clave varchar)
RETURNS @resultado TABLE (estado int,dato varchar,usuario varchar,usuarioID varchar) AS
BEGIN

	DECLARE
	@v_registro cursor,
	@v_respuesta varchar,
	@v_estado integer;

	BEGIN
		set @v_registro = (
			select
			u.personal_dni,
			u.estado,
			u.clave,
			CONVERT(varchar,p.nombre + ' ' + p.apellidoPaterno + ' ' + p.apellidoMaterno) as nombre,
			u.usuarioID as cod
			from
			usuario u inner join personal p on (u.personal_dni = p.dni)
			where
			u.personal_dni = @p_dni
		);

		set @v_estado = 500; --Error

		if ( EXISTS (@v_registro)) BEGIN
			if @v_registro.clave = @p_clave
				if @v_registro.estado = 'I'
				@v_respuesta = 'Usuario Inactivo'
				else
				@v_estado = 200
				@v_respuesta = @v_registro.personal_dni
				else
				@v_respuesta = 'Contraseņa Incorrecta'
				else
				@v_respuesta = 'El usuario no existe'
		END
	END;

if @v_estado = 200
insert @resultado select @v_estado, @v_respuesta, convert(varchar,@v_registro.nombre), @v_registro.cod
else
insert @resultado select @v_estado, @v_respuesta, convert(varchar,'-'), 0

END