﻿delete from producto where productoid=117;

select * from producto

select * from f_agregar_producto('SDCZ50-008G-B35   ','sdfasdfdsaf', 'sdfasdfdsaf', 'sdfasdfdsaf', 4)

CREATE OR REPLACE FUNCTION public.f_agregar_producto(
    IN p_codigoproducto character varying,
    IN p_modelo character varying,
    IN p_descripcion character varying,
    IN p_capacidad character varying,
    IN p_tipocategoriaid integer)
  RETURNS TABLE(productoid int, codigoproducto varchar) AS
$BODY$
	DECLARE

		v_productoid integer;
		v_codigoproducto varchar;
		
	begin	
		begin

			select rtrim(producto.codigoproducto,' ') into v_codigoproducto from producto where producto.codigoproducto @@ rtrim(p_modelo,' ');
			
			if v_codigoproducto is not null then 
			RAISE EXCEPTION 'El producto (%) ya se encuentra agregado', p_codigoproducto;
			end if;

			select numero + 1 into v_productoid from correlativo where tabla = 'producto';

			INSERT INTO public.producto(productoid,codigoproducto, modelo, descripcion, capacidad, tipocategoriaid)
                        VALUES (v_productoid, p_codigoproducto, p_modelo,p_descripcion, p_capacidad, p_tipocategoriaid);

                        update correlativo set numero = numero + 1 where tabla = 'producto';
			
		EXCEPTION
			when others then --Cuando ocurra algun error, muestra un mensaje
				RAISE EXCEPTION '%', SQLERRM;
			
		end;

		return query select v_productoid,p_codigoproducto;
	end;
	
$BODY$
  LANGUAGE plpgsql VOLATILE